package acessoBanco;

import java.sql.*;
import javax.swing.JOptionPane;

public class PedidoDB {
    private PreparedStatement select;
    private PreparedStatement update;
    private ResultSet rs;
    private final Conexao con;
    ClienteDB objCliente = new ClienteDB();
    ProdutoDB objProduto = new ProdutoDB();
    
    public PedidoDB(){
        con = new Conexao();
   }
    
    public int Incluir(int produto,int quantidade, double total) throws SQLException{
        int res = 0;
         
        
        try{
            if (con.getConn()!= null){
                
                String SQL = "Insert into pedidoProduto(fkProduto,fkPedido,quantidade,Total)";
                SQL = SQL + "Values (?,(SELECT MAX(ID)FROM PEDIDO),?,?)";
                update = con.getConn().prepareStatement(SQL);
                update.setInt(1,produto);
                update.setInt(2, quantidade);
                update.setDouble(3, total);

                update.executeUpdate();
                update.close();
                res = 1;
            }   
            
        } catch( SQLException cnfe ){
            JOptionPane.showMessageDialog(null, "EROR: " + cnfe.getMessage(), "Cadastro de Cliente", JOptionPane.INFORMATION_MESSAGE);
            //System.out.println("EROR");
            res=2;
        }            
        return res;
    }
    
    public int incluirPedido(String previsao,String cliente){
        int res = 0 ;
        int idCliente = objCliente.getID(cliente);
        
        try{
            if (con.getConn()!= null){
                String SQL = "Insert into pedido(previsaoEntrega,fkCliente)     ";
                SQL = SQL + "Values (?,?)";
                update = con.getConn().prepareStatement(SQL);
                update.setString(1, previsao);
                update.setInt(2,idCliente);
                
                update.executeUpdate();
                update.close();
                res = 1;
            }   
            
        } catch( Exception cnfe ){
            JOptionPane.showMessageDialog(null, "EROR: " + cnfe.getMessage(), "Cadastro de Cliente", JOptionPane.INFORMATION_MESSAGE);
            //System.out.println("EROR");
            res=2;
        }            
        return res;
    }
}
