package acessoBanco;

import java.sql.*;
import java.util.*;
import javax.swing.JOptionPane;

public class ClienteDB {
    
    private PreparedStatement select;
    private PreparedStatement update;
    private ResultSet rs;
    private final Conexao con;
    
    public ClienteDB(){
        con = new Conexao();
   }
    
    public int Incluir(String nome, String CPF, String end){
        int res = 0 ;
        try{
            if (con.getConn()!= null){
                String SQL = "Insert into Cliente(Nome,CPF,Endereco)  ";
                SQL = SQL + "Values (?,?,?)";
                update = con.getConn().prepareStatement(SQL);
                update.setString(1, nome);
                update.setString(2, CPF);
                update.setString(3, end);

                update.executeUpdate();
                update.close();
                res = 1;
            }   
            
        } catch( SQLException cnfe ){
            //System.out.println();
            JOptionPane.showMessageDialog(null, "EROR: " + cnfe.getMessage(), "Cadastro de Cliente", JOptionPane.INFORMATION_MESSAGE);
            res=2;
        }            
       
        return res;
    }
   
    public List getCliente() throws SQLException{

        List<String> clientes = new ArrayList<>();
        
            try{
                if (con.getConn()!= null){
                    ResultSet rs1; 
                    String query = "SELECT * FROM `cliente`";
                    Statement stm = con.conn.createStatement();
                    rs1 = stm.executeQuery(query);
                    
                    while(rs1.next())
                    {
                        clientes.add(rs1.getString("Nome"));
                    }
                    con.conn.close();
                }
            }
            catch( SQLException cnfe ){
            System.out.println("EROR" + cnfe.getMessage());
            }
        return clientes;
    }
    
    public int getID(String Nome){

    int id = 0;

        try{
            if (con.getConn()!= null){
                ResultSet rs1; 
                String query = "SELECT id FROM `cliente` WHERE NOME = '" + Nome + "'";
                Statement stm = con.conn.createStatement();
                rs1 = stm.executeQuery(query);

                while(rs1.next())
                {
                    id = rs1.getInt("id");
                }
                con.conn.close();
            }
        }
        catch( Exception ex ){
        System.out.println("EROR" + ex.getMessage());
        }
    return id;
    }      
}
