package acessoBanco;

import java.sql.*;
import java.util.*;
import javax.swing.JOptionPane;

public class ProdutoDB {
    
    private PreparedStatement select;
    private PreparedStatement update;
    private ResultSet rs;
    private final Conexao con;
    
    public ProdutoDB(){
        con = new Conexao();
   }
    
    public int Incluir(String descricao, double valor){
        int res = 0 ;
        try{
            if (con.getConn()!= null){
                String SQL = "Insert into Produto(Descricao,Valor)";
                SQL = SQL + "Values (?,?)";
                update = con.getConn().prepareStatement(SQL);
                update.setString(1, descricao);
                update.setDouble(2, valor);

                update.executeUpdate();
                update.close();
                res = 1;
            }   
            
        } catch( SQLException cnfe ){
            JOptionPane.showMessageDialog(null, "EROR: " + cnfe.getMessage(), "Cadastro de Cliente", JOptionPane.INFORMATION_MESSAGE);
            //System.out.println("EROR");
            res=2;
        }            
       
        return res;
    }
    
    public List getProduto() throws SQLException{

    List<String> Produtos = new ArrayList<>();

        try{
                if (con.getConn()!= null){
                    ResultSet rs1; 
                    String query = "SELECT * FROM `produto`";
                    Statement stm = con.conn.createStatement();
                    rs1 = stm.executeQuery(query);
                    
                    while(rs1.next())
                    {
                    Produtos.add(rs1.getString("Descricao"));
                    }
                    con.conn.close();
                }
            }
            catch( SQLException cnfe ){
            System.out.println("EROR" + cnfe.getMessage());
            }
    return Produtos;
    }
   
    public double  getValor(String descricao) throws SQLException{
        ResultSet rs1; 
        double valor;
        String query = "Select valor from produto Where descricao = '" + descricao + "'";
        rs1 = null;
        try{
            if (con.getConn()!= null){
                Statement stm = con.conn.createStatement();
                rs1 = stm.executeQuery(query);
                while(rs1.next()){
                    valor = rs1.getDouble("valor");
                    return valor;
                }
            }else{
                return 0;
            }
        }catch(Exception e){
            System.out.println("EROR: " + e.getMessage());
            return 0;
        }
        return 0;
    }
    
    public int getID(String Descricao){

    int id = 0;

        try{
            if (con.getConn()!= null){
                ResultSet rs1; 
                String query = "SELECT id FROM `produto` WHERE Descricao = '" + Descricao + "'";
                Statement stm = con.conn.createStatement();
                rs1 = stm.executeQuery(query);

                while(rs1.next())
                {
                    id = rs1.getInt("id");
                }
                con.conn.close();
            }
        }
        catch( Exception ex ){
        System.out.println("EROR" + ex.getMessage());
        }
    return id;
    }   
}
