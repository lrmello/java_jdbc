package projeto.cadastro.de.pedidos;

public class frmHome extends javax.swing.JFrame {

    public frmHome() {
        this.setLocationRelativeTo(null);
        initComponents();
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jButton2 = new javax.swing.JButton();
        jButton3 = new javax.swing.JButton();
        jButton4 = new javax.swing.JButton();
        jLabel4 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        jLabel6 = new javax.swing.JLabel();
        jLabel5 = new javax.swing.JLabel();
        jSeparator1 = new javax.swing.JSeparator();
        jSeparator2 = new javax.swing.JSeparator();
        jSeparator3 = new javax.swing.JSeparator();
        background = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setTitle("Cadastro de Pedidos");
        setBackground(new java.awt.Color(102, 102, 102));
        setMaximumSize(new java.awt.Dimension(500, 550));
        setMinimumSize(new java.awt.Dimension(500, 550));
        setPreferredSize(new java.awt.Dimension(500, 600));
        setResizable(false);
        setSize(new java.awt.Dimension(500, 400));
        getContentPane().setLayout(null);

        jButton2.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Images/1495484800_vector_65_13.png"))); // NOI18N
        jButton2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton2ActionPerformed(evt);
            }
        });
        getContentPane().add(jButton2);
        jButton2.setBounds(20, 220, 130, 140);

        jButton3.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Images/1495484719_vector_65_02.png"))); // NOI18N
        jButton3.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton3ActionPerformed(evt);
            }
        });
        getContentPane().add(jButton3);
        jButton3.setBounds(20, 90, 130, 100);

        jButton4.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Images/1495484775_vector_65_12.png"))); // NOI18N
        jButton4.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton4ActionPerformed(evt);
            }
        });
        getContentPane().add(jButton4);
        jButton4.setBounds(20, 390, 130, 100);

        jLabel4.setFont(new java.awt.Font("BankGothic Lt BT", 0, 36)); // NOI18N
        jLabel4.setForeground(new java.awt.Color(0, 204, 255));
        jLabel4.setText("Produtos");
        getContentPane().add(jLabel4);
        jLabel4.setBounds(240, 260, 200, 50);

        jLabel3.setFont(new java.awt.Font("BankGothic Lt BT", 0, 36)); // NOI18N
        jLabel3.setForeground(new java.awt.Color(0, 204, 255));
        jLabel3.setText("Cadastro de Pedidos");
        getContentPane().add(jLabel3);
        jLabel3.setBounds(80, 10, 340, 50);

        jLabel6.setFont(new java.awt.Font("BankGothic Lt BT", 0, 36)); // NOI18N
        jLabel6.setForeground(new java.awt.Color(0, 204, 255));
        jLabel6.setText("Pedidos");
        getContentPane().add(jLabel6);
        jLabel6.setBounds(240, 430, 170, 50);

        jLabel5.setFont(new java.awt.Font("BankGothic Lt BT", 0, 36)); // NOI18N
        jLabel5.setForeground(new java.awt.Color(0, 204, 255));
        jLabel5.setText("Clientes");
        getContentPane().add(jLabel5);
        jLabel5.setBounds(250, 110, 170, 50);
        getContentPane().add(jSeparator1);
        jSeparator1.setBounds(0, 370, 500, 10);
        getContentPane().add(jSeparator2);
        jSeparator2.setBounds(0, 70, 500, 10);
        getContentPane().add(jSeparator3);
        jSeparator3.setBounds(0, 200, 500, 10);

        background.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Images/Background Projeto.png"))); // NOI18N
        getContentPane().add(background);
        background.setBounds(0, 70, 500, 440);

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void jButton2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton2ActionPerformed
        // TODO add your handling code here:
        frmCadastroProduto Produto = new frmCadastroProduto();
        Produto.setDefaultCloseOperation(1);
        Produto.setVisible(true);
    }//GEN-LAST:event_jButton2ActionPerformed

    private void jButton3ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton3ActionPerformed
        // TODO add your handling code here:
        frmCadastroCliente Cliente = new frmCadastroCliente();
        Cliente.setDefaultCloseOperation(1);
        Cliente.setVisible(true);

    }//GEN-LAST:event_jButton3ActionPerformed

    private void jButton4ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton4ActionPerformed
        // TODO add your handling code here:
        frmCadastroPedido Pedido = new frmCadastroPedido();
        Pedido.setDefaultCloseOperation(1);
        Pedido.setVisible(true);
    }//GEN-LAST:event_jButton4ActionPerformed

    public static void main(String args[]) {

        java.awt.EventQueue.invokeLater(() -> {
            new frmHome().setVisible(true);
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JLabel background;
    private javax.swing.JButton jButton2;
    private javax.swing.JButton jButton3;
    private javax.swing.JButton jButton4;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JSeparator jSeparator1;
    private javax.swing.JSeparator jSeparator2;
    private javax.swing.JSeparator jSeparator3;
    // End of variables declaration//GEN-END:variables
}
